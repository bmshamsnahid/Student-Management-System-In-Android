package com.bmshamsnahid.student_management_system;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class List_Records extends ListActivity{
	
	String names[];
	ArrayAdapter<String> array_adapter;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		initialize();
	}
	
	private void initialize() {
		get_names();
		set_adapter();
	}

	private void set_adapter() {
		array_adapter = new ArrayAdapter<String>(List_Records.this, android.R.layout.simple_list_item_1, names);
		setListAdapter(array_adapter);
	}

	private void get_names() {
		FileOperationHandler foh = new FileOperationHandler(this);
		String str = foh.read_file("HostFile");
		String[] names_temp = str.split("@");
		String temp = "";
		int l = names_temp.length;
		for(int i=0; i<l; i++) {
			if(foh.file_exist(names_temp[i])) {
				if(temp.contains(names_temp[i]) == false)
				temp += names_temp[i];
				temp += "@";
			}
		}
		//str = foh.read_file("HostFile");
		l = temp.length();
		temp = temp.substring(1, l-1);
		names = temp.split("@");
	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);
		Intent intent = new Intent(List_Records.this, Show_Information.class);
		intent.putExtra("string_key", names[position]);
		startActivity(intent);
	}
}
