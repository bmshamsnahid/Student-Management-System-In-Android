package com.bmshamsnahid.student_management_system;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

public class Show_Information extends Activity{
	
	Bundle extras;
	String name, id;
	String[] id_array;
	TextView tv_name, tv_id, tv_session, tv_hall, tv_department, tv_roll, tv_contact_no;
	FileOperationHandler foh;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.show_information);
		foh = new FileOperationHandler(this);
		initialize();
	}

	void st(String str) {
		Toast.makeText(this, str, Toast.LENGTH_SHORT).show();
	}
	
	private void initialize() {
		name_id_initialize();
		text_view_initialize();
		
		name_set();
		id_set();
		session_set();
		hall_set();
		department_set();
		roll_set();
		contact_no_set();
	}

	private void contact_no_set() {
		tv_contact_no.setText(foh.read_file(name + "_contact"));
	}

	private void roll_set() {
		tv_roll.setText(id_array[7] + id_array[8]);
	}

	private void department_set() {
		String str = id_array[5] + id_array[6];
		if(str.contentEquals("54")) tv_department.setText("CSE Department");
		else if(str.contentEquals("49")) tv_department.setText("APEE Department");
		else tv_department.setText("Unknown Department");
	}

	private void hall_set() {
		String str = id_array[3] + id_array[4];
		if(str.contentEquals("01")) tv_department.setText("Sher-e-Bangla Fazlul Haque Hall");
		else if(str.contentEquals("02")) tv_hall.setText("Shah Makhdum Hall");
		else if(str.contentEquals("03")) tv_hall.setText("Nawab Abdul Latif Hall");
		else if(str.contentEquals("04")) tv_hall.setText("Syed Amir Ali Hall");
		else if(str.contentEquals("05")) tv_hall.setText("Shahid Shamsuzzoha Hall");
		else if(str.contentEquals("06")) tv_hall.setText("Shahid Habibur Rahman Hall");
		else if(str.contentEquals("07")) tv_hall.setText("Motihar Hall");
		else if(str.contentEquals("08")) tv_hall.setText("Madar Baksh Hall");
		else if(str.contentEquals("09")) tv_hall.setText("Shahid Sohrawardi Hall");
		else if(str.contentEquals("10")) tv_hall.setText("Shahid Ziaur Rahman Hall");
		else if(str.contentEquals("11")) tv_hall.setText("Bangabandhu Sheikh Mujibur Rahman Hall");
		else if(str.contentEquals("12")) tv_hall.setText("Monnujan Hall");
		else if(str.contentEquals("13")) tv_hall.setText("Begum Rokeya Hall");
		else if(str.contentEquals("14")) tv_hall.setText("Tapashi Rabeya Hall");
		else if(str.contentEquals("15")) tv_hall.setText("Begum Khaleda Zia Hall");
		else if(str.contentEquals("16")) tv_hall.setText("Rahamatunnesa Hall");
		else tv_hall.setText("Unknown Hall");
	}

	private void session_set() {
		String str = id_array[1] + id_array[2];
		int num = Integer.parseInt(str);
		num--;
		str = "20" + String.valueOf(num) + "-" + str;
		tv_session.setText(str);
	}

	private void id_set() {
		tv_id.setText(id);
	}

	private void name_set() {
		tv_name.setText(name);
	}

	private void text_view_initialize() {
		tv_name = (TextView) findViewById(R.id.show_information_text_view_name);
		tv_id = (TextView) findViewById(R.id.show_information_text_view_id);
		tv_session = (TextView) findViewById(R.id.show_information_text_view_session);
		tv_hall = (TextView) findViewById(R.id.show_information_text_view_hall);
		tv_department = (TextView) findViewById(R.id.show_information_text_view_department);
		tv_roll = (TextView) findViewById(R.id.show_information_text_view_roll);
		tv_contact_no = (TextView) findViewById(R.id.show_information_text_view_contact_no);
	}

	private void name_id_initialize() {
		extras = getIntent().getExtras();
		name = extras.getString("string_key");
		st(name);
		
		id = foh.read_file(name + "_roll");
		id_array = id.split("");
	}
}
