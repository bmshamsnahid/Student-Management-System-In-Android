package com.bmshamsnahid.student_management_system;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class Add_Records extends Activity implements OnClickListener{
	
	Button bt_add_records;
	EditText et_first_name, et_last_name, et_roll_number, et_mobile_number;
	TextView tv_status;
	String name;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.add_records);
		initialize();
	}

	private void initialize() {
		string_initialize();
		button_initialize();
		edit_text_initialize();
		text_view_initialize();
	}

	private void string_initialize() {
		name = "";
	}

	private void text_view_initialize() {
		tv_status = (TextView) findViewById(R.id.add_records_text_view_status);
	}

	private void edit_text_initialize() {
		et_first_name = (EditText) findViewById(R.id.add_records_edit_text_first_name);
		et_last_name = (EditText) findViewById(R.id.add_records_edit_text_last_name);
		et_roll_number = (EditText) findViewById(R.id.add_records_edit_text_roll_number);
		et_mobile_number = (EditText) findViewById(R.id.add_records_edit_text_mobile_number);
	}

	private void button_initialize() {
		bt_add_records = (Button) findViewById(R.id.add_records_button_add_records);
		bt_add_records.setOnClickListener(this);
	}
	
	void st(String str) {
		Toast.makeText(this, str, Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.add_records_button_add_records:
			//st("add records");
			
			if(check_roll()) {
				String info = get_string();
				
				FileOperationHandler foh = new FileOperationHandler(this);
				foh.open_file(name, info);
				foh.open_file(name + "_roll", et_roll_number.getText().toString());
				foh.open_file(name + "_contact", et_mobile_number.getText().toString());
				
				String str = foh.read_file("HostFile");
				foh.delete_file("HostFile");
				str += ("@" + name);
				foh.open_file("HostFile", str);
				
				/*str = foh.read_file("HostFile");
				tv_status.setText(str);*/
			} else {
				st("Cant Add: Incorrect Roll Number Format");
			}
			
			break;

		default:
			break;
		}
	}

	private boolean check_roll() {
		String str = et_roll_number.getText().toString();
		if(str.length() != 8) return false;
		int l = str.length();
		for(int i=0; i<l; i++) {
			if(is_num(str.charAt(i)) == false) return false;
		}
		return true;
	}

	private boolean is_num(char ch) {
		if(ch == '0' || ch == '1' || ch == '2' || ch == '3' || ch == '4' || ch == '5' || 
				ch == '6' || ch == '7' || ch == '8' || ch == '9') return true;
		
		return false;
	}

	private String get_string() {
		String first_name, last_name, roll_number, mobile_number;
		first_name = last_name = roll_number = mobile_number = "";
		
		first_name = et_first_name.getText().toString();
		last_name = et_last_name.getText().toString();
		roll_number = et_roll_number.getText().toString();
		mobile_number = et_mobile_number.getText().toString();
		
		name = first_name + " " + last_name;
		
		return "Name: " + first_name + " " + last_name + "\nRoll Number: "
		+ roll_number + "\nMobile Number: " + mobile_number;
	}
}
