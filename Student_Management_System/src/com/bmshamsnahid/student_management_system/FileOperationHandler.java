package com.bmshamsnahid.student_management_system;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.view.Menu;
import android.widget.TextView;
import android.widget.Toast;

public class FileOperationHandler{
	String file_name_array[];
	String file_name_string;
	static final int READ_BLOCK_SIZE = 100;
	Context c;
	
	public FileOperationHandler(Context c) {
		this.c = c;
		file_name_array = new String[10000];
		file_name_string = "";
		ready_file();
	}
	
	public void ready_file() {
		if(!file_exist("HostFile")) {
			open_file("HostFile", "");
		}
		file_name_string = read_file("HostFile");
		file_name_array = file_name_string.split("@");
	}
	
	public boolean file_exist(String filename) {
		//ready_file();
		File file = new File(c.getFilesDir() + "/" + filename);
		if(file.exists()) {
			//Toast.makeText(this, "Exist", Toast.LENGTH_SHORT).show();
			return true;
		} else {
			//Toast.makeText(this, "Not Exist", Toast.LENGTH_SHORT).show();
			return false;
		}
	}
	
	public void open_file(String name, String file_info) {
		FileOutputStream outputStream;
		try {
			outputStream = c.openFileOutput(name, Context.MODE_PRIVATE);
			outputStream.write(file_info.getBytes());
			outputStream.close();
		} catch (Exception e) {
			//Toast.makeText(this, e.toString(), Toast.LENGTH_SHORT).show();
		}
	}
	
	public String read_file(String filename) {
		try {
			FileInputStream fileIn=c.openFileInput(filename.toString());
			InputStreamReader InputRead= new InputStreamReader(fileIn);
			
			char[] inputBuffer= new char[READ_BLOCK_SIZE];
			String s="";
			int charRead;
			
			while ((charRead=InputRead.read(inputBuffer))>0) {
				String readstring=String.copyValueOf(inputBuffer,0,charRead);
				s += readstring;				
			}
			InputRead.close();
			//Toast.makeText(this, s, Toast.LENGTH_SHORT).show();
			return s;
		} catch (Exception e) {
			//Toast.makeText(this, e.toString(), Toast.LENGTH_SHORT).show();
			return e.toString();
		}
	}
	
	public boolean delete_file(String filename) {
		try {
			c.deleteFile(filename);
		} catch(Exception e) {
			return false;
		}
		return true;
	}
}
