package com.bmshamsnahid.student_management_system;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

public class Host_Screen extends Activity implements OnClickListener{
	
	Button bt_add_records, bt_search_records, bt_list_records, bt_modify_records, bt_delete_records;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.host_screen);
		initialize();
	}

	private void initialize() {
		//delete_all_files();
		button_initialize();
	}

	private void delete_all_files() {
		FileOperationHandler foh = new FileOperationHandler(this);
		String str = foh.read_file("HostFile");
		String[] names_temp = str.split("@");
		String temp = "";
		int l = names_temp.length;
		for(int i=0; i<l; i++) {
			if(foh.file_exist(names_temp[i])) {
				if(temp.contains(names_temp[i]) == false)
				temp += names_temp[i];
				temp += "@";
			}
		}
		//str = foh.read_file("HostFile");
		l = temp.length();
		temp = temp.substring(1, l-1);
		String names[] = temp.split("@");
		l = names.length;
		for(int i=0; i<l; i++) {
			foh.delete_file(names[i]);
			foh.delete_file(names[i] + "_roll");
			foh.delete_file(names[i] + "_contact");
		}
	}

	private void button_initialize() {
		bt_add_records = (Button) findViewById(R.id.host_screen_button_add_records);
		bt_search_records = (Button) findViewById(R.id.host_screen_button_search_records);
		bt_list_records = (Button) findViewById(R.id.host_screen_button_list_records);
		bt_modify_records = (Button) findViewById(R.id.host_screen_button_modify_records);
		bt_delete_records = (Button) findViewById(R.id.host_screen_button_delete_records);
		
		bt_add_records.setOnClickListener(this);
		bt_search_records.setOnClickListener(this);
		bt_list_records.setOnClickListener(this);
		bt_modify_records.setOnClickListener(this);
		bt_delete_records.setOnClickListener(this);
	}
	
	void st(String str) {
		Toast.makeText(this, str, Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.host_screen_button_add_records:
			//st("add");
			startActivity(new Intent(Host_Screen.this, Add_Records.class));
			break;
		case R.id.host_screen_button_search_records:
			//st("search");
			startActivity(new Intent(Host_Screen.this, Search_Records.class));
			break;
		case R.id.host_screen_button_list_records:
			//st("list");
			startActivity(new Intent(Host_Screen.this, List_Records.class));
			break;
		case R.id.host_screen_button_modify_records:
			//st("modify");
			startActivity(new Intent(Host_Screen.this, Modify_Records.class));
			break;
		case R.id.host_screen_button_delete_records:
			//st("delete");
			startActivity(new Intent(Host_Screen.this, Delete_Records.class));
			break;
		}
	}

}
