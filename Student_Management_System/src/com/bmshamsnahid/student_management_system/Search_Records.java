package com.bmshamsnahid.student_management_system;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class Search_Records extends Activity implements OnClickListener{
	
	EditText et_first_name, et_last_name;
	Button bt_search;
	TextView tv_status;
	String name;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.search_records);
		
		initialize();
	}
	
	private void initialize() {
		button_initialize();
		edit_text_initialize();
		text_view_initialize();
		string_initialize();
	}

	private void string_initialize() {
		name = "";
	}

	private void text_view_initialize() {
		tv_status = (TextView) findViewById(R.id.search_records_text_view_status);
	}

	private void edit_text_initialize() {
		et_first_name = (EditText) findViewById(R.id.search_records_edit_text_first_name);
		et_last_name = (EditText) findViewById(R.id.search_records_edit_text_last_name);
	}

	private void button_initialize() {
		bt_search = (Button) findViewById(R.id.search_records_button_search_records);
		bt_search.setOnClickListener(this);
	}

	void st(String str) {
		Toast.makeText(this, str, Toast.LENGTH_SHORT).show();
	}
	
	String getstring() {
		String first_name, last_name;
		first_name = et_first_name.getText().toString();
		last_name = et_last_name.getText().toString();
		return first_name + " " + last_name;
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.search_records_button_search_records:
			st("search");
			FileOperationHandler foh = new FileOperationHandler(this);
			String str = foh.read_file("HostFile");
			if(str.contains(getstring())) {
				str = foh.read_file(getstring());
				tv_status.setText(str);
			}
			break;

		default:
			break;
		}
	}
}
