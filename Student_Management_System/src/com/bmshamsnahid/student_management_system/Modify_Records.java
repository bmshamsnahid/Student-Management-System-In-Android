package com.bmshamsnahid.student_management_system;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class Modify_Records extends Activity implements OnClickListener{
	
	Button bt_modify_records;
	EditText et_first_name, et_last_name, et_roll_number, et_mobile_number;
	TextView tv_status;
	String name;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.modify_records);
		initialize();
	}

	private void initialize() {
		string_initialize();
		button_initialize();
		edit_text_initialize();
		text_view_initialize();
	}
	
	private void string_initialize() {
		name = "";
	}

	private void text_view_initialize() {
		tv_status = (TextView) findViewById(R.id.modify_records_text_view_status);
	}

	private void edit_text_initialize() {
		et_first_name = (EditText) findViewById(R.id.modify_records_edit_text_first_name);
		et_last_name = (EditText) findViewById(R.id.modify_records_edit_text_last_name);
		et_roll_number = (EditText) findViewById(R.id.modify_records_edit_text_roll_number);
		et_mobile_number = (EditText) findViewById(R.id.modify_records_edit_text_mobile_number);
	}

	private void button_initialize() {
		bt_modify_records = (Button) findViewById(R.id.modify_records_button_modify_records);
		bt_modify_records.setOnClickListener(this);
	}
	
	void st(String str) {
		Toast.makeText(this, str, Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.modify_records_button_modify_records:
			//st("modify records");
			//tv_status.setText(get_string());
			
			String info = get_string();
			FileOperationHandler foh = new FileOperationHandler(this);
			
			if(foh.file_exist(name)) {
				foh.delete_file(name);
				foh.delete_file(name + "_roll");
				
				foh.open_file(name, info);
				foh.open_file(name + "_roll", et_roll_number.getText().toString());
				foh.open_file(name + "_contact", et_mobile_number.getText().toString());
			} else {
				foh.open_file(name, info);
				foh.open_file(name + "_roll", et_roll_number.getText().toString());
				foh.open_file(name + "_contact", et_mobile_number.getText().toString());
				
				String str = foh.read_file("HostFile");
				foh.delete_file("HostFile");
				str += ("@" + name);
				foh.open_file("HostFile", str);
			}
			
			/*str = foh.read_file("HostFile");
			tv_status.setText(str);*/
			
			break;

		default:
			break;
		}
	}

	private String get_string() {
		String first_name, last_name, roll_number, mobile_number;
		first_name = last_name = roll_number = mobile_number = "";
		
		first_name = et_first_name.getText().toString();
		last_name = et_last_name.getText().toString();
		roll_number = et_roll_number.getText().toString();
		mobile_number = et_mobile_number.getText().toString();
		
		name = first_name + " " + last_name;
		
		return "Name: " + first_name + " " + last_name + "\nRoll Number: "
		+ roll_number + "\nMobile Number: " + mobile_number;
	}
}
